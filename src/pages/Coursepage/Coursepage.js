import React from 'react';
import ListCourse from './ListCourse';
import { listLogoSourseData } from '../../data/sourseData';

export default function Coursepage() {
    return (
        <div className="course-container">
            <div className="item-name bg-gray-600 text-white px-10 py-10">
                <h2 className="text-3xl font-semibold">KHÓA HỌC</h2>
                <p className="text-sm font-sans">BẮT ĐẦU HÀNH TRÌNH NÀO !!</p>
            </div>
            <div className="list-logo mt-10  flex justify-center items-center">
                <div className="row">
                    <div className="logos  columns-6 gap-0 text-white ">
                        {listLogoSourseData.map((item) => {
                            return (
                                <div className={`item py-8 px-8 h-[160px] ${item.color}`}>
                                    <h2 className="text-center font-medium">{item.name}</h2>
                                    <h2 className="text-center font-medium text-3xl my-2 ">{item.icon}</h2>
                                    <p className="text-center font-medium text-xl">{item.num}</p>
                                </div>
                            );
                        })}
                    </div>
                </div>
            </div>
            <div className=" container px-28 pt-20">
                <ListCourse />
            </div>
        </div>
    );
}
