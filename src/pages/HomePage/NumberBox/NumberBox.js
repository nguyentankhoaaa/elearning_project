import React from 'react';
import CountUp from 'react-countup';
import './NumberBox.css';
import VisibilitySensor from 'react-visibility-sensor';

export default function NumberBox() {
    return (
        <div className="boxNumberContainer mt-5">
            <div className="grid grid-flow-row sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-4 gap-4">
                <div className="boxNumber">
                    <div>
                        <img src="./image/003-students.e1a7c67b.png" alt="" className="imgIcon" />
                    </div>
                    <div className="counter textNumber">
                        <CountUp end={9000} duration={4} redraw={true}>
                            {({ countUpRef, start }) => (
                                <VisibilitySensor onChange={start} delayedCall>
                                    <span ref={countUpRef} />
                                </VisibilitySensor>
                            )}
                        </CountUp>
                    </div>
                    <p className="textNumberTitle">Học viên</p>
                </div>
                <div className="boxNumber">
                    <div>
                        <img src="./image/001-timetable.0e009173.png" alt="" className="imgIcon" />
                    </div>
                    <div className="counter textNumber">
                        <CountUp end={1000} duration={4} redraw={true}>
                            {({ countUpRef, start }) => (
                                <VisibilitySensor onChange={start} delayedCall>
                                    <span ref={countUpRef} />
                                </VisibilitySensor>
                            )}
                        </CountUp>
                    </div>
                    <p className="textNumberTitle">Khoá học</p>
                </div>
                <div className="boxNumber">
                    <div>
                        <img src="./image/002-hourglass.548810be.png" alt="" className="imgIcon" />
                    </div>
                    <div className="counter textNumber">
                        <CountUp end={33200} duration={4} redraw={true}>
                            {({ countUpRef, start }) => (
                                <VisibilitySensor onChange={start} delayedCall>
                                    <span ref={countUpRef} />
                                </VisibilitySensor>
                            )}
                        </CountUp>
                    </div>
                    <p className="textNumberTitle">Giờ học</p>
                </div>
                <div className="boxNumber">
                    <div>
                        <img src="./image/004-teacher.5bbd6eec.png" alt="" className="imgIcon" />
                    </div>
                    <div className="counter textNumber">
                        <CountUp end={400} duration={4} redraw={true}>
                            {({ countUpRef, start }) => (
                                <VisibilitySensor onChange={start} delayedCall>
                                    <span ref={countUpRef} />
                                </VisibilitySensor>
                            )}
                        </CountUp>
                    </div>
                    <p className="textNumberTitle">Giảng viên</p>
                </div>
            </div>
        </div>
    );
}
