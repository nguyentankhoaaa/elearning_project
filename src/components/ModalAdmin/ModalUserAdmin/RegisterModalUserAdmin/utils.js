export const waitingCourse = [
    {
        title: 'STT',
        dataIndex: 'stt',
    },
    {
        title: 'Tên khóa học',
        dataIndex: 'tenKhoaHoc',
        align: 'center',
    },
    {
        title: 'Chờ xác nhận',
        dataIndex: 'action',
        align: 'center',
    },
];

export const approvedCourse = [
    {
        title: 'STT',
        dataIndex: 'stt',
        align: 'center',
    },
    {
        title: 'Tên khóa học',
        dataIndex: 'tenKhoaHoc',
        align: 'center',
    },
    {
        title: 'Chờ xác nhận',
        dataIndex: 'action',
        align: 'center',
    },
];
