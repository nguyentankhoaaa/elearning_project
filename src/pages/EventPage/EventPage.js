import React from 'react'
import robot_img from "../../assets/img/event_img/robot.png"
import instructor5 from "../../assets/img/event_img/instrutor5.jpg"
import instructor6 from "../../assets/img/event_img/instrutor6.jpg"
import instructor7 from "../../assets/img/event_img/instrutor7.jpg"
import instructor8 from "../../assets/img/event_img/instrutor8.jpg"
import instructor9 from "../../assets/img/event_img/instrutor9.jpg"
import instructor10 from "../../assets/img/event_img/instrutor10.jpg"
import instructor11 from "../../assets/img/event_img/instrutor11.jpg"
import instructor12 from "../../assets/img/event_img/instrutor12.jpg"
import FB from "../../assets/img/event_img/meta.jpg"
import amazon from "../../assets/img/event_img/amazon.jpg"
import microsoft from "../../assets/img/event_img/microsoft.jpg"
import google from "../../assets/img/event_img/Google-logo.jpg"
export default function EventPage() {
  return (
    <div className='eventPage '>
        <div className="nav-bar">
            <div className="content pl-14 pr-80  py-40">
<div className='time-title columns-4 '>
    <div className='event'>
        <p className='text-6xl   font-bold text-yellow-500'>00</p>
        <p className='text-white  font-medium text-2xl'>NGÀY</p>
    </div>
    <div className='event'>
        <p className='text-6xl   font-bold text-orange-500'>00</p>
        <p className='text-white  font-medium text-2xl'>GIỜ</p>
    </div>
    <div className='event'>
        <p className='text-6xl   font-bold text-pink-700'>00</p>
        <p className='text-white  font-medium text-2xl'>PHÚT</p>
    </div>
    <div className='event'>
        <p className='text-6xl   font-bold text-purple-700'>00</p>
        <p className='text-white  font-medium text-2xl'>GIÂY</p>
    </div>

        </div>
<div className='main-content pl-10'>
<p className='text-white font-bold text-4xl mt-5'>SỰ KIỆN CÔNG NGHỆ LỚN NHẤT 2021</p>
<p className='text-white font-bold text-xl mt-5'>20 - 25 THÁNG 12, 2022, VIỆT NAM</p>
</div>
            </div>
        </div>
      <div className="event-container">
        <div className="first-title  columns-2 py-10 px-10">
       <div className='robot-img flex justify-center items-center'>
     <img src={robot_img} alt="" style={{width:"70%"}}/>
       </div>
       <div className="content mt-14 ">
        <h1 className='text-3xl font-bold text-pink-800'>SỰ KIỆN CÔNG NGHỆ DÀNH CHO STARTUP</h1>
        <p className='font-medium mt-3 mb-5'>Nơi gặp gỡ của những tư tưởng lớn</p>
        <p className='text-gray-500 font-medium'>Innovatube Frontier Summit (IFS) là sự kiện đầu tiên tại Việt Nam tập trung
             vào cả bốn mảng tiêu biểu của công nghệ tiên phong, bao gồm Artificial Intel
             ligence (trí tuệ nhân tạo), Internet of Things (Internet vạn vật), Blockchain (Chuỗi khối)
             và Augmented reality/Virtual Reality (Thực tế tăng cường/Thực tế ảo)</p>
        <div className='buttons flex  items-center mt-5'>
            <button className='px-3 py-2 bg-cyan-500 text-white font-medium'>THAM GIA</button>
            <button className='px-3 py-2 bg-yellow-500 text-white font-medium ml-5'>TÌM HIỂU THÊM</button>

        </div>
       </div>
        </div>
        <div className="creator-event py-28">
          <h2 className=' text-center text-3xl py-5 font-bold text-yellow-500'>CÁC NHÀ ĐỒNG SÁNG TẠO</h2>
            <div className=" container  columns-4 px-16 gap-5">
<div className='item-img '>
<img src={instructor5} alt="" />
<p className='font-bold text-white'>NGUYỄN NHẬT</p>
<p className=' text-white'>CEO TECHVIET PRODUCTION</p>
</div>
<div className='item-img'>
<img src={instructor6} alt="" />
<p className='font-bold text-white'>NGUYỄN NHẬT NAM</p>
<p className=' text-white'>CEO TECHVIET PRODUCTION</p>
</div>
<div className='item-img'>
<img src={instructor7} alt="" />
<p className='font-bold text-white'>NGUYỄN NHẬT NAM</p>
<p className=' text-white'>CEO TECHVIET PRODUCTION</p>
</div>
<div className='item-img'>
<img src={instructor8} alt="" />
<p className='font-bold text-white'>NGUYỄN NAM</p>
<p className=' text-white'>CEO TECHVIET PRODUCTION</p>
</div>
<div className='item-img'>
<img src={instructor9} alt="" />
<p className='font-bold text-white'>JONHNY ĐẶNG</p>
<p className=' text-white'>CEO TECHVIET PRODUCTION</p>
</div>
<div className='item-img'>
<img src={instructor10} alt="" />
<p className='font-bold text-white'>NGÔ HENRY</p>
<p className=' text-white'>CEO TECHVIET PRODUCTION</p>
</div>
<div className='item-img'>
<img src={instructor11} alt="" />
<p className='font-bold text-white'>VƯƠNG PHẠM VN</p>
<p className=' text-white'>CEO TECHVIET PRODUCTION</p>
</div>
<div className='item-img'>
<img src={instructor12} alt="" />
<p className='font-bold text-white'>KHOA PUG</p>
<p className=' text-white'>CEO TECHVIET PRODUCTION</p>
</div>
        
          </div>
        </div>
        <div className="donors-event py-20">
        <h2 className=' text-center text-3xl py-5 font-bold text-yellow-500'>NHÀ TÀI TRỢ CHƯƠNG TRÌNH</h2>
        <div className="container columns-4 px-16 gap-5 py-5">
  <div className='item-img' >
   <img src={FB} alt=""  />
   <p className='text-xl font-medium text-center'>FACEBOOK</p>
  </div>
  <div className='item-img'>
   <img src={amazon} alt="" />
   <p className='text-xl font-medium text-center'>AMAZON</p>
  </div>
  <div className='item-img'>
   <img src={microsoft} alt="" />
   <p className='text-xl font-medium text-center'>GOOGLE</p>
  </div>
  <div className='item-img'>
   <img src={google} alt="" />
   <p className='text-xl font-medium text-center'>MICROSOFT </p>
  </div>
        </div>
        </div>

      </div>


    </div>
  )
}
