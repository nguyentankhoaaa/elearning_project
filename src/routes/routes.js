import AdminLayout from '../layouts/AdminLayout';
import UserLayout from '../layouts/UserLayout';
import AdminCoursesPage from '../pages/AdminPage/AdminCoursesPage/AdminCoursesPage';
import AdminUsersPage from '../pages/AdminPage/AdminUsersPage/AdminUsersPage';
import BlogPage from '../pages/BlogPage/BlogPage';
import Coursepage from '../pages/Coursepage/Coursepage';
import DetailPage from '../pages/DetailPage/DetailPage';
import EventPage from '../pages/EventPage/EventPage';
import InforPage from '../pages/InforPage/InforPage';
import LoginPage from '../pages/LoginPage/LoginPage';
import RegisterPage from '../pages/RegisterPage1/RegisterPage';
import NotFoundPage from '../pages/NotFoundPage/NotFoundPage';
import HomePage from '../pages/HomePage/HomePage';
import CourseListFollowCategory from '../pages/CourseListFollowCategory/CourseListFollowCategory';
import Personal from '../pages/Personal/Personal';
import { Navigate } from 'react-router';

export const routes = [
    {
        path: '/admin',
        component: <AdminLayout Component={AdminUsersPage} />,
    },

    {
        path: '/admin/quanlynguoidung',
        component: <AdminLayout Component={AdminUsersPage} />,
    },

    {
        path: '/admin/quanlykhoahoc',
        component: <AdminLayout Component={AdminCoursesPage} />,
    },
    {
        path: '/login',
        component: <LoginPage />,
    },
    {
        path: '/register',
        component: <RegisterPage />,
    },
    {
        path: '/course',
        component: <UserLayout Component={Coursepage} />,
    },
    {
        path: '/detail/:id',
        component: <UserLayout Component={DetailPage} />,
    },
    {
        path: '/event',
        component: <UserLayout Component={EventPage} />,
    },
    {
        path: '/information',
        component: <UserLayout Component={InforPage} />,
    },
    {
        path: '/blog',
        component: <UserLayout Component={BlogPage} />,
    },

    {
        path: '/*',
        component: <UserLayout Component={NotFoundPage} />,
    },

    {
        path: '/home',
        component: <UserLayout Component={HomePage} />,
    },

    {
        path: '/danhmuckhoahoc/:courses',
        component: <UserLayout Component={CourseListFollowCategory} />,
    },

    {
        path: '/thong-tin-ca-nhan/',
        component: <UserLayout Component={Personal} />,
    },

    {
        path: '/',
        component: <Navigate to="/login" replace />,
    },
];
