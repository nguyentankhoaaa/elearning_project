import { Avatar, Card } from 'antd';
import './ItemCourse.css';
const { Meta } = Card;
const ItemCourse = ({ data }) => (
    <div className="cardEffect mt-4 cardGlobalRes">
        <a href={`/detail/${data.maKhoaHoc}`} className="cardGlobal moveSubCard">
            <img src={data.hinhAnh} alt="" />
            <span className="stickerCard">{data.tenKhoaHoc}</span>
            <div className="cardBodyGlobal">
                <h6>Lập trình hiện đang là xu hướng trên toàn thế giới...</h6>
                <div className="cardIcon">
                    <div className="imgCardFooter">
                        <img src="./image/avatar2.bb9626e2.png" alt="" />
                    </div>
                    <span class="colorCardTitle">Elon Musk</span>
                </div>
            </div>
            <div className="cardFooter">
                <div>
                    <p>
                        800.000
                        <sup>đ</sup>
                    </p>
                    <p>
                        400.000
                        <sup>đ</sup>
                    </p>
                </div>
                <div>
                    <i className="fas fa-star mr-1 textStar"></i>
                    <span className="textStar">4.9</span>
                    <span className="colorCardTitle">(7480)</span>
                </div>
            </div>
            <div className="subCard">
                <div className="subCardHead">
                    <img src="./image/emoji.6d1b7051.png" alt="" />
                    <span className="ml-1 colorCardTitle">Elon Musk Richard</span>
                </div>
                <h6>BOOTCAMP - LẬP TRÌNH FULL STACK TỪ ZERO ĐẾN CÓ VIỆC</h6>
                <p class="colorCardTitle">
                    Đã có hơn 6200 bạn đăng kí học và có việc làm thông qua chương trình đào tạo Bootcamp Lập trình
                    Front End chuyên nghiệp. Khóa học 100% thực hành cường độ cao theo dự án thực tế và kết nối doanh
                    nghiệp hỗ trợ tìm việc ngay sau khi học...
                </p>
                <div className="cardIcon">
                    <span>
                        <i class="far fa-clock iconOclock"></i> 8 giờ
                    </span>
                    <span>
                        <i class="far fa-calendar-alt iconCalendar"></i> 4 tuần
                    </span>
                    <span>
                        <i class="fas fa-signal iconLevel"></i> Tất cả
                    </span>
                </div>
                <button class="btnGlobal btnSubCard">
                    <a href="/chitiet/aJava">Xem chi tiết</a>
                </button>
            </div>
        </a>
    </div>
);
export default ItemCourse;
