import React from 'react';
import { NavLink } from 'react-router-dom';
import { CourseIcon, HomeIcon, UserIcon } from '../Icons/Icons';

export default function SidebarAdmin() {
    return (
        <section className="flex flex-col bg-sidebar-bg rounded-lg px-2 py-4 leading-8 text-base font-medium w-[80px]">
            <NavLink
                to="/home"
                className="mb-10 py-4 hover:text-[#41b294] hover:cursor-pointer hover:bg-white hover:rounded-lg transition-all"
            >
                <HomeIcon className="w-full" />
            </NavLink>

            <NavLink
                to="/admin/quanlynguoidung"
                className="block py-4 text-center mb-10 hover:text-[#41b294] hover:cursor-pointer hover:bg-white hover:rounded-lg transition-all"
            >
                <UserIcon className="w-full" />
                Quản lý
                <br />
                người dùng
            </NavLink>

            <NavLink
                to="/admin/quanlykhoahoc"
                className="block text-center py-4 hover:text-[#41b294] hover:cursor-pointer hover:bg-white hover:rounded-lg transition-all"
            >
                <CourseIcon className="w-full" />
                Quản lý
                <br />
                khóa học
            </NavLink>
        </section>
    );
}
