import React, { useEffect, useState } from 'react';
import { courceServ } from '../../../services/courseService';
import ItemCourse from './ItemCourse';
import ItemCourseRef from './ItemCourseRef';

import styles from './ItemCourse.css';
export default function ListCourse() {
    const [course, setCourse] = useState([]);
    useEffect(() => {
        courceServ
            .getCourseFullList()
            .then((res) => {
                setCourse(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    const [webCourse, setWebCourse] = useState([]);
    useEffect(() => {
        courceServ
            .getCourseList('web')
            .then((res) => {
                setWebCourse(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    return (
        <>
            <div className="homepage">
                <div className="grid grid-flow-row sm:grid-cols-1 xl:grid-cols-2 sliderHome">
                    <div className="">
                        <div className="sliderRight">
                            <div className=""></div>
                            <div>
                                <img className="sliderMainImg" src="./image/slider2.f170197b.png" alt="" />
                                <img
                                    className="sliderSubImg sliderCodeImg"
                                    src="./image/code_slider.8c12bbb4.png"
                                    alt=""
                                />
                                <img
                                    className="sliderSubImg sliderMesImg"
                                    src="./image/message_slider.6835c478.png"
                                    alt=""
                                />
                                <img className="sliderSubImg sliderCloudImg" src="./image/clouds.15eb556c.png" alt="" />
                                <img
                                    className="sliderSubImg sliderCloud2Img"
                                    src="./image/clouds.15eb556c.png"
                                    alt=""
                                />
                                <img
                                    className="sliderSubImg sliderCloud3Img"
                                    src="./image/clouds.15eb556c.png"
                                    alt=""
                                />
                            </div>
                        </div>
                    </div>
                    <div className="sloganBox">
                        <div className="triangleTopRight"></div>
                        <div className="smallBox smallboxLeftTop"></div>
                        <div className="smallBox smallboxRightTop"></div>
                        <div className="smallBox smallboxRightBottom"></div>
                        <div className="smallBox smallboxRightBottom doubleBox"></div>
                        <div className="sloganContainer">
                            <div>
                                <img src="./image/paper_plane.93dfdbf5.png" className="sliderPlaneImg" />
                                <h1>Chào mừng</h1>
                                <h1>đến với môi trường</h1>
                                <h1>
                                    V<span>learning</span>
                                </h1>
                                <button className="btnGlobal btnSlider mt-4">Bắt đầu nào</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="infoCourseBox">
                    <div className="infoCourseHome grid grid-cols-none">
                        <div className="infoItemHome infoLargeItem">
                            <div className="infoItemContent">
                                <h3>Khoá học</h3>
                                <p>
                                    <span>Học qua dự án thực tế</span>, học đi đôi với hành, không lý thuyết lan man,
                                    phân tích cội nguồn của vấn đề, xây dựng từ các ví dụ nhỏ đến thực thi một dự án lớn
                                    ngoài thực tế để học viên học xong làm được ngay
                                </p>
                                <ul>
                                    <li>
                                        <i className="fas fa-check"></i>
                                        <span>Hơn 1000 bài tập và dự án thực tế</span>
                                    </li>
                                    <li>
                                        <i className="fas fa-check"></i>
                                        <span>Công nghệ cập nhật mới nhất</span>
                                    </li>
                                    <li>
                                        <i className="fas fa-check"></i>
                                        <span>Hình ảnh, ví dụ, bài giảng sinh động trực quan</span>
                                    </li>
                                    <li>
                                        <i className="fas fa-check"></i>
                                        <span>Tư duy phân tích, giải quyết vấn đề trong dự án</span>
                                    </li>
                                    <li>
                                        <i className="fas fa-check"></i>
                                        <span>Học tập kinh nghiệm, qui trình làm dự án, các qui chuẩn trong dự án</span>
                                    </li>
                                    <li>
                                        <i className="fas fa-check"></i>
                                        <span>Cơ hội thực tập tại các công ty lớn như FPT, Microsoft</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="infoItemHome infoSmallItemA">
                            <div className="infoItemContent">
                                <h3>Lộ trình phù hợp</h3>
                                <ul>
                                    <li>
                                        <i className="fas fa-check"></i>
                                        <span>Lộ trình bài bản từ zero tới chuyên nghiệp, nâng cao</span>
                                    </li>
                                    <li>
                                        <i className="fas fa-check"></i>
                                        <span>Học, luyện tập code, kỹ thuật phân tích, soft skill</span>
                                    </li>
                                    <li>
                                        <i className="fas fa-check"></i>
                                        <span>Huấn luyện để phát triển năng lực và niềm đam mê lập trình</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="infoItemHome infoSmallItemB">
                            <div className="infoItemContent">
                                <h3>Hệ thống học tập</h3>
                                <ul>
                                    <li>
                                        <i className="fas fa-check"></i>
                                        <span>
                                            Tự động chấm điểm trắc nghiệm và đưa câu hỏi tùy theo mức độ học viên
                                        </span>
                                    </li>
                                    <li>
                                        <i className="fas fa-check"></i>
                                        <span>Thống kê lượt xem video, làm bài, điểm số theo chu kỳ</span>
                                    </li>
                                    <li>
                                        <i className="fas fa-check"></i>
                                        <span>
                                            Thống kê, so sánh khả năng học của các học viên cùng level để đưa ra mục
                                            tiêu học tập
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="infoItemHome infoSmallItemA">
                            <div className="infoItemContent">
                                <h3>Giảng viên</h3>
                                <ul>
                                    <li>
                                        <i className="fas fa-check"></i>
                                        <span>Tương tác cùng mentor và giảng viên qua phần thảo luận</span>
                                    </li>
                                    <li>
                                        <i className="fas fa-check"></i>
                                        <span>Review code và đưa ra các nhận xét góp ý</span>
                                    </li>
                                    <li>
                                        <i className="fas fa-check"></i>
                                        <span>Chấm điểm tương tác thảo luận giữa các học viên</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="infoItemHome infoSmallItemC">
                            <div className="infoItemContent">
                                <h3>Chứng nhận</h3>
                                <ul>
                                    <li>
                                        <i className="fas fa-check"></i>
                                        <span>Chấm bài và có thể vấn đáp trực tuyến để review</span>
                                    </li>
                                    <li>
                                        <i className="fas fa-check"></i>
                                        <span>
                                            Hệ thống của chúng tôi cũng tạo ra cho bạn một CV trực tuyến độc đáo
                                        </span>
                                    </li>
                                    <li>
                                        <i className="fas fa-check"></i>
                                        <span>Kết nối CV của bạn đến với các đối tác của V learning</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                {/* Khoá học phổ biến */}
                <div className="courseHome">
                    <h6>
                        <a href="">Khoá học phổ biến</a>
                    </h6>
                </div>
                <div className="grid grid-flow-row sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-4 gap-4 mt-4">
                    {course.slice(0, 4).map((item) => {
                        return <ItemCourse data={item} key={item.maKhoaHoc} />;
                    })}
                </div>
                {/* Khoá học tham khảo */}
                <div className="mt-5">
                    <h6>
                        <a href="">Khoá học tham khảo</a>
                    </h6>
                </div>
                <div className="grid grid-flow-row sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-4 gap-4 mt-4">
                    {course.slice(6, 10).map((item) => {
                        return <ItemCourseRef data={item} key={item.maKhoaHoc} />;
                    })}
                </div>
                {/* Khoá học Front End React JS */}
                <div className="mt-5">
                    <h6>
                        <a href="">Khoá học Front End React JS</a>
                    </h6>
                </div>
                <div className="grid grid-flow-row sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-4 gap-4">
                    {webCourse.slice(0, 4).map((item) => {
                        return <ItemCourseRef data={item} key={item.maKhoaHoc} />;
                    })}
                </div>
            </div>
        </>
    );
}
