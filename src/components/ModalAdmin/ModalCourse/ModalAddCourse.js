import { Divider, Form, Modal, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { setSuccessOff, setSuccessOn } from '../../../react-toolkit/successNotifySlice';
import { setWarningOff, setWarningOn } from '../../../react-toolkit/warningNotifySlice';
import { adminServices } from '../../../services/adminServices';
import { CardIcon, UserIcon } from '../../Icons/Icons';

export default function ModalAddCourse({ open, handleCancel, category, fetchData }) {
    const dispatch = useDispatch();

    const onFinish = (values) => {
        const newValues = { ...values, biDanh: values.tenKhoaHoc.split(' ').join('-') };

        adminServices
            .addCourse(newValues)
            .then((res) => {
                dispatch(setSuccessOn('Thêm thành công'));
                setTimeout(() => {
                    dispatch(setSuccessOff());
                }, 2000);
                handleCancel();
                fetchData();
            })
            .catch((err) => {
                dispatch(setWarningOn(err.response.data));
                setTimeout(() => {
                    dispatch(setWarningOff());
                }, 2000);
            });
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <>
            <Modal width={800} open={open} onCancel={handleCancel} closable={false} footer={null} className="top-2">
                <Typography.Title className="text-center">Thông tin Khóa học</Typography.Title>
                <Form onFinish={onFinish} onFinishFailed={onFinishFailed} autoComplete="off">
                    <div className="grid grid-cols-2 gap-x-4">
                        <Form.Item
                            name="maKhoaHoc"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập mã khóa học!',
                                },
                            ]}
                        >
                            <div className="flex items-center justify-center border border-gray-200 rounded">
                                <div className="w-10 h-[38px] flex items-center justify-center border-r bg-[#E9ECEF]">
                                    <span>
                                        <UserIcon className="h-4 w-4" />
                                    </span>
                                </div>
                                <input
                                    type="text"
                                    placeholder="Mã Khóa Học"
                                    className="flex-grow px-3 py-[6px] border rounded-r focus-visible:outline-none focus:border-red-200"
                                />
                            </div>
                        </Form.Item>

                        <Form.Item
                            name="maDanhMucKhoaHoc"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng chọn danh mục!',
                                },
                            ]}
                        >
                            <div className="flex items-center justify-center border border-gray-200 rounded">
                                <div className="w-10 h-[38px] flex items-center justify-center border-r bg-[#E9ECEF]">
                                    <span>
                                        <CardIcon className="h-4 w-4" />
                                    </span>
                                </div>
                                <select className="flex-grow px-3 py-[6px]  rounded-r focus-visible:outline-none focus:border-red-200">
                                    <option value="">Danh mục khóa học</option>
                                    {category?.map((item) => {
                                        return (
                                            <option key={item.maDanhMuc} value={item.maDanhMuc}>
                                                {item.tenDanhMuc}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                        </Form.Item>

                        <Form.Item
                            name="danhGia"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập đánh giá!',
                                },
                            ]}
                        >
                            <div className="flex items-center justify-center border border-gray-200 rounded">
                                <div className="w-10 h-[38px] flex items-center justify-center border-r bg-[#E9ECEF]">
                                    <span>
                                        <UserIcon className="h-4 w-4" />
                                    </span>
                                </div>
                                <input
                                    type="text"
                                    placeholder="Đánh giá"
                                    className="flex-grow px-3 py-[6px] border rounded-r focus-visible:outline-none focus:border-red-200"
                                />
                            </div>
                        </Form.Item>

                        <Form.Item
                            name="taiKhoanNguoiTao"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập tài khoản!',
                                },
                            ]}
                        >
                            <div className="flex items-center justify-center border border-gray-200 rounded">
                                <div className="w-10 h-[38px] flex items-center justify-center border-r bg-[#E9ECEF]">
                                    <span>
                                        <UserIcon className="h-4 w-4" />
                                    </span>
                                </div>
                                <input
                                    type="text"
                                    placeholder="Người tạo"
                                    className="flex-grow px-3 py-[6px] border rounded-r focus-visible:outline-none focus:border-red-200"
                                />
                            </div>
                        </Form.Item>

                        <Form.Item
                            name="maNhom"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập mã nhóm!',
                                },
                            ]}
                        >
                            <div className="flex items-center justify-center border border-gray-200 rounded">
                                <div className="w-10 h-[38px] flex items-center justify-center border-r bg-[#E9ECEF]">
                                    <span>
                                        <CardIcon className="h-4 w-4" />
                                    </span>
                                </div>
                                <select className="flex-grow px-3 py-[6px]  rounded-r focus-visible:outline-none focus:border-red-200">
                                    <option value="">Mã nhóm</option>
                                    <option value="GP01">GP01</option>
                                    <option value="GP02">GP02</option>
                                    <option value="GP03">GP03</option>
                                    <option value="GP04">GP04</option>
                                    <option value="GP05">GP05</option>
                                </select>
                            </div>
                        </Form.Item>

                        <Form.Item
                            name="tenKhoaHoc"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập tên khóa học!',
                                },
                            ]}
                        >
                            <div className="flex items-center justify-center border border-gray-200 rounded">
                                <div className="w-10 h-[38px] flex items-center justify-center border-r bg-[#E9ECEF]">
                                    <span>
                                        <UserIcon className="h-4 w-4" />
                                    </span>
                                </div>
                                <input
                                    type="text"
                                    placeholder="Tên khóa học"
                                    className="flex-grow px-3 py-[6px] border rounded-r focus-visible:outline-none focus:border-red-200"
                                />
                            </div>
                        </Form.Item>

                        <Form.Item
                            name="ngayTao"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập ngày tạo!',
                                },
                            ]}
                        >
                            <div className="flex items-center justify-center border border-gray-200 rounded">
                                <div className="w-10 h-[38px] flex items-center justify-center border-r bg-[#E9ECEF]">
                                    <span>
                                        <UserIcon className="h-4 w-4" />
                                    </span>
                                </div>
                                <input
                                    type="text"
                                    placeholder="Ngày tạo"
                                    className="flex-grow px-3 py-[6px] border rounded-r focus-visible:outline-none focus:border-red-200"
                                />
                            </div>
                        </Form.Item>

                        <Form.Item
                            name="luotXem"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập lượt xem!',
                                },
                            ]}
                        >
                            <div className="flex items-center justify-center border border-gray-200 rounded">
                                <div className="w-10 h-[38px] flex items-center justify-center border-r bg-[#E9ECEF]">
                                    <span>
                                        <UserIcon className="h-4 w-4" />
                                    </span>
                                </div>
                                <input
                                    type="text"
                                    placeholder="Lượt xem"
                                    className="flex-grow px-3 py-[6px] border rounded-r focus-visible:outline-none focus:border-red-200"
                                />
                            </div>
                        </Form.Item>

                        <Form.Item
                            name="moTa"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập mô tả!',
                                },
                            ]}
                        >
                            <div className="flex items-center h-20 justify-center border border-gray-200 rounded">
                                <div className="w-10 h-full flex items-center justify-center border-r bg-[#E9ECEF]">
                                    <span className="h-full flex items-center">
                                        <UserIcon className="h-4 w-4" />
                                    </span>
                                </div>
                                <textarea
                                    className="flex-grow h-full px-3 py-[6px] border focus-visible:outline-0 focus:border-red-200"
                                    placeholder="Mô tả"
                                ></textarea>
                            </div>
                        </Form.Item>

                        <Form.Item
                            name="hinhAnh"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập hình ảnh!',
                                },
                            ]}
                        >
                            <div className="flex items-center justify-center border border-gray-200 rounded">
                                <div className="w-10 h-[38px] flex items-center justify-center border-r bg-[#E9ECEF]">
                                    <span>
                                        <UserIcon className="h-4 w-4" />
                                    </span>
                                </div>
                                <input
                                    accept="image/*"
                                    type="file"
                                    placeholder="Hình ảnh"
                                    className="flex-grow px-3 focus-visible:outline-none focus:shadow-none"
                                />
                            </div>
                        </Form.Item>
                    </div>
                    <Divider className="border border-gray-300" />
                    <div className="text-right">
                        <button
                            htmltype="submit"
                            className="px-3 py-[6px] rounded bg-[#41B294] text-white hover:bg-red-400"
                        >
                            Thêm khóa học
                        </button>
                        <button
                            className="px-3 py-[6px] rounded bg-red-700 text-white ml-2 hover:bg-red-800"
                            onClick={handleCancel}
                        >
                            Đóng
                        </button>
                    </div>
                </Form>
            </Modal>
        </>
    );
}
