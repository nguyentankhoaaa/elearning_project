import React from 'react';
import timeLogo from '../../assets/img/blog_img/thoigian.jpg';
import tailwind from '../../assets/img/blog_img/tailwindCss.png';
import material_ui from '../../assets/img/blog_img/material-UI.jpg';
import html from '../../assets/img/blog_img/html5-logo.png';
import component from '../../assets/img/blog_img/React-Components.webp';
import logo_material from '../../assets/img/blog_img/logo_materialTheme.png';
import batDB from '../../assets/img/blog_img/BatDongBo.jpg';
import typeScript from '../../assets/img/blog_img/basicType.jpg';
import ava_1 from '../../assets/img//event_img/instrutor5.jpg';
import ava_2 from '../../assets/img//event_img/instrutor6.jpg';
import ava_3 from '../../assets/img//event_img/instrutor7.jpg';
import { Card } from 'antd';
export default function BlogPage() {
    return (
        <div className="blogPage">
            <div className="item-name bg-gray-600 text-white px-10 py-10">
                <h2 className="text-3xl font-semibold">BLOG</h2>
                <p className="text-sm font-sans">THÔNG TIN CÔNG NGHỆ SỐ !!</p>
            </div>
            <div className="container">
                <h2 className="text-xl font-bold text-gray-700 px-16 pt-8 pb-4">
                    <i className="fas fa-bullhorn  text-pink-500" /> Danh Sách Khóa Học
                </h2>
                <div className="blog-container px-16 flex  ">
                    <div className="item-left w-2/3  columns-2 gap-7">
                        <div className="item">
                            <img src={timeLogo} style={{ height: '300px', width: '500px' }} alt="" />
                            <div className="content">
                                <p className="font-medium text-xl py-4">Thời gian và động lực</p>
                                <div className="title flex justify-between">
                                    <p>
                                        <i className="far fa-thumbs-up text-green-500"></i> <span>300</span>
                                        <i className="far fa-comment ml-3 text-green-500"></i> <span>500</span>
                                        <i className="fas fa-eye ml-3 text-green-500"></i> <span>200</span>
                                    </p>
                                    <p className="font-medium">
                                        Đăng bởi <span className="text-pink-400">Jhony Đặng</span>
                                    </p>
                                </div>
                                <p className="text-gray-500 my-3">
                                    Có lẽ cũng rất lâu rồi mà tôi chưa đụng đến thứ được gọi là "timetable". Hay dân dã
                                    hơn thì ngườ i ta hay gọi là "Lịch thường nhật",...
                                </p>
                                <button className="blog-btn text-white bg-yellow-500 font-medium px-2 py-1 rounded-sm">
                                    XEM THÊM
                                </button>
                            </div>
                        </div>
                        <div className="item">
                            <img src={tailwind} style={{ height: '300px', width: '500px' }} alt="" />
                            <div className="content">
                                <p className="font-medium text-xl py-4">Tailwind css và cách cài đặt cơ bản</p>
                                <div className="title flex justify-between">
                                    <p>
                                        <i className="far fa-thumbs-up text-green-500"></i> <span>300</span>
                                        <i className="far fa-comment ml-3 text-green-500"></i> <span>500</span>
                                        <i className="fas fa-eye ml-3 text-green-500"></i> <span>200</span>
                                    </p>
                                    <p className="font-medium">
                                        Đăng bởi <span className="text-pink-400">Jhony Đặng</span>
                                    </p>
                                </div>
                                <p className="text-gray-500 my-3">
                                    Có lẽ cũng rất lâu rồi mà tôi chưa đụng đến thứ được gọi là "timetable". Hay dân dã
                                    hơn thì người ta hay gọi là "Lịch thường nhật",...
                                </p>
                                <button className="blog-btn text-white bg-yellow-500 font-medium px-2 py-1 rounded-sm">
                                    XEM THÊM
                                </button>
                            </div>
                        </div>
                        <div className="item">
                            <img src={html} style={{ height: '300px', width: '500px' }} alt="" />
                            <div className="content">
                                <p className="font-medium text-xl py-4">Cấu trúc cơ bản trong HTML</p>
                                <div className="title flex justify-between">
                                    <p>
                                        <i className="far fa-thumbs-up text-green-500"></i> <span>300</span>
                                        <i className="far fa-comment ml-3 text-green-500"></i> <span>500</span>
                                        <i className="fas fa-eye ml-3 text-green-500"></i> <span>200</span>
                                    </p>
                                    <p className="font-medium">
                                        Đăng bởi <span className="text-pink-400">Jhony Đặng</span>
                                    </p>
                                </div>
                                <p className="text-gray-500 my-3">
                                    Custom theme Material UI với TypeScript đơn giản, hiệu quả...
                                </p>
                                <button className="blog-btn text-white bg-yellow-500 font-medium px-2 py-1 rounded-sm">
                                    XEM THÊM
                                </button>
                            </div>
                        </div>
                        <div className="item">
                            <img src={material_ui} style={{ height: '300px', width: '500px' }} alt="" />
                            <div className="content">
                                <p className="font-medium text-xl py-4">Material UI custom theme với TypeScript</p>
                                <div className="title flex justify-between">
                                    <p>
                                        <i className="far fa-thumbs-up text-green-500"></i> <span>300</span>
                                        <i className="far fa-comment ml-3 text-green-500"></i> <span>500</span>
                                        <i className="fas fa-eye ml-3 text-green-500"></i> <span>200</span>
                                    </p>
                                    <p className="font-medium">
                                        Đăng bởi <span className="text-pink-400">Jhony Đặng</span>
                                    </p>
                                </div>
                                <p className="text-gray-500 my-3">
                                    Trắc hẳn ai cũng biết một trang web thì không thể nào thiếu đi HTML và HTML làm nên
                                    cấu trúc của một trang web...
                                </p>
                                <button className="blog-btn text-white bg-yellow-500 font-medium px-2 py-1 rounded-sm">
                                    XEM THÊM
                                </button>
                            </div>
                        </div>
                        <div className="item">
                            <img src={component} style={{ height: '300px', width: '500px' }} alt="" />
                            <div className="content">
                                <p className="font-medium text-xl py-4">
                                    Cách tạo một component nhanh chóng chỉ bằng 3 ký tự
                                </p>
                                <div className="title flex justify-between">
                                    <p>
                                        <i className="far fa-thumbs-up text-green-500"></i> <span>300</span>
                                        <i className="far fa-comment ml-3 text-green-500"></i> <span>500</span>
                                        <i className="fas fa-eye ml-3 text-green-500"></i> <span>200</span>
                                    </p>
                                    <p className="font-medium">
                                        Đăng bởi <span className="text-pink-400">Jhony Đặng</span>
                                    </p>
                                </div>
                                <p className="text-gray-500 my-3">
                                    Tạo một component nhiều lúc cũng khá mất nhiều thời gian nên mình xin giới thiệu
                                    extention này cho mọi người nhé...
                                </p>
                                <button className="blog-btn text-white bg-yellow-500 font-medium px-2 py-1 rounded-sm">
                                    XEM THÊM
                                </button>
                            </div>
                        </div>
                        <div className="item">
                            <img src={logo_material} style={{ height: '300px', width: '500px' }} alt="" />
                            <div className="content">
                                <p className="font-medium text-xl py-4">Material UI custom theme với TypeScript</p>
                                <div className="title flex justify-between">
                                    <p>
                                        <i className="far fa-thumbs-up text-green-500"></i> <span>300</span>
                                        <i className="far fa-comment ml-3 text-green-500"></i> <span>500</span>
                                        <i className="fas fa-eye ml-3 text-green-500"></i> <span>200</span>
                                    </p>
                                    <p className="font-medium">
                                        Đăng bởi <span className="text-pink-400">Jhony Đặng</span>
                                    </p>
                                </div>
                                <p className="text-gray-500 my-3">
                                    Như các bạn đã biết chúng ta sẽ sử dụng target="_blank" cho thẻ a để khi người dùng
                                    click vô sẽ mở liên kết trên một tab mới...
                                </p>
                                <button className="blog-btn text-white bg-yellow-500 font-medium px-2 py-1 rounded-sm">
                                    XEM THÊM
                                </button>
                            </div>
                        </div>
                        <div className="item">
                            <img src={batDB} style={{ height: '300px', width: '500px' }} alt="" />
                            <div className="content">
                                <p className="font-medium text-xl py-4">Xử lý bất đồng bộ trong Javascript (phần 2)</p>
                                <div className="title flex justify-between">
                                    <p>
                                        <i className="far fa-thumbs-up text-green-500"></i> <span>300</span>
                                        <i className="far fa-comment ml-3 text-green-500"></i> <span>500</span>
                                        <i className="fas fa-eye ml-3 text-green-500"></i> <span>200</span>
                                    </p>
                                    <p className="font-medium">
                                        Đăng bởi <span className="text-pink-400">Jhony Đặng</span>
                                    </p>
                                </div>
                                <p className="text-gray-500 my-3">
                                    Async/await là cơ chế giúp bạn thực thi các thao tác bất đồng bộ một cách tuần tự
                                    hơn , giúp đoạn code nhìn qua tưởng như đồng...
                                </p>
                                <button className="blog-btn text-white bg-yellow-500 font-medium px-2 py-1 rounded-sm">
                                    XEM THÊM
                                </button>
                            </div>
                        </div>
                        <div className="item">
                            <img src={typeScript} style={{ height: '300px', width: '500px' }} alt="" />
                            <div className="content">
                                <p className="font-medium text-xl py-4">
                                    TyperScrip là gì, Vì sao nên dùng TyperScript
                                </p>
                                <div className="title flex justify-between">
                                    <p>
                                        <i className="far fa-thumbs-up text-green-500"></i> <span>300</span>
                                        <i className="far fa-comment ml-3 text-green-500"></i> <span>500</span>
                                        <i className="fas fa-eye ml-3 text-green-500"></i> <span>200</span>
                                    </p>
                                    <p className="font-medium">
                                        Đăng bởi <span className="text-pink-400">Jhony Đặng</span>
                                    </p>
                                </div>
                                <p className="text-gray-500 my-3">
                                    Async/await là cơ chế giúp bạn thực thi các thao tác bất đồng bộ một cách tuần tự
                                    hơn , giúp đoạn code nhìn qua tưởng như đồng...
                                </p>
                                <button className="blog-btn text-white bg-yellow-500 font-medium px-2 py-1 rounded-sm">
                                    XEM THÊM
                                </button>
                            </div>
                        </div>
                    </div>

                    <div className="item-right">
                        <Card
                            title="Các chủ đề được đề xuất"
                            bordered={false}
                            className="card border-t-4 text-gray-500  border-green-500 text-center shadow-xl"
                            style={{
                                width: 500,
                                marginLeft: 20,
                            }}
                        >
                            <div className="title text-left text-base">
                                <p>Front-end / Mobile apps</p>
                                <p>UI / UX / Design</p>
                                <p>BACK-END</p>
                                <p>Thư viện</p>
                                <p>Chia sẻ người trong nghề</p>
                                <p>Châm ngôn IT</p>
                                <p>Chủ đề khác</p>
                            </div>
                        </Card>
                        <Card
                            title="Bài đăng được đề xuất"
                            bordered={false}
                            className="card mt-20 border-t-4 text-gray-500  border-green-500 text-center shadow-3xl"
                            style={{
                                width: 500,
                                marginLeft: 20,
                            }}
                        >
                            <div className="content text-left text-base">
                                <div className="item">
                                    <p className="font-medium">Routing trong reactjs</p>
                                    <p className="text-gray-500 my-2">
                                        Chúng ta sẽ cùng nhau tìm hiểu cách routing trong reactjs...
                                    </p>
                                    <div className="flex items-center mt-3">
                                        <img
                                            src={ava_1}
                                            className="rounded-3xl"
                                            style={{ width: '40px', height: '40px' }}
                                            alt=""
                                        />
                                        <p className="text-lg text-gray-500 ml-2 ">Nguyên Văn</p>
                                    </div>
                                </div>
                                <div className="item mt-20">
                                    <p className="font-medium">Lập trình hướng đối tượng oop</p>
                                    <p className="text-gray-500 my-2">
                                        Chúng ta sẽ cùng nhau tìm hiểu cách oop trong reactjs...
                                    </p>
                                    <div className="flex items-center mt-3">
                                        <img
                                            src={ava_2}
                                            className="rounded-3xl"
                                            style={{ width: '40px', height: '40px' }}
                                            alt=""
                                        />
                                        <p className="text-lg text-gray-500 ml-2 ">Nguyên Văn Vũ</p>
                                    </div>
                                </div>
                                <div className="item mt-20">
                                    <p className="font-medium">Xử Lý Bất Đồng Bộ Trong Javascript</p>
                                    <p className="text-gray-500 my-2">
                                        Chắc chắn khi lập trình, bạn sẽ có các công việc cần thời gian delay (gọi API,
                                        lấy dữ liệu từ Database, đọc/ghi file,...). Và đây..
                                    </p>
                                    <div className="flex items-center mt-3">
                                        <img
                                            src={ava_3}
                                            className="rounded-3xl"
                                            style={{ width: '40px', height: '40px' }}
                                            alt=""
                                        />
                                        <p className="text-lg text-gray-500 ml-2 ">Nguyên Minh</p>
                                    </div>
                                </div>
                            </div>
                        </Card>
                    </div>
                </div>
            </div>
        </div>
    );
}
