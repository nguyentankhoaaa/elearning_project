import { Divider, Form, Modal, Typography } from 'antd';
import React from 'react';
import { useDispatch } from 'react-redux';
import { setSuccessOff, setSuccessOn } from '../../../react-toolkit/successNotifySlice';
import { setWarningOff, setWarningOn } from '../../../react-toolkit/warningNotifySlice';
import { adminServices } from '../../../services/adminServices';
import { BookIcon, CardIcon, EmailIcon, KeyIcon, PhoneIcon, UserIcon } from '../../Icons/Icons';

export default function ModalAddUserAdmin({ open, handleCancel, fetchData }) {
    const dispatch = useDispatch();

    const onFinish = (values) => {
        const newUserInfo = { ...values, maNhom: 'GP01' };
        adminServices
            .addUser(newUserInfo)
            .then((res) => {
                handleCancel();
                dispatch(setSuccessOn('Đã thêm thành công'));
                setTimeout(() => {
                    dispatch(setSuccessOff());
                }, 2000);
                fetchData();
                handleCancel();
            })
            .catch((err) => {
                dispatch(setWarningOn(err.response.data));
                setTimeout(() => {
                    dispatch(setWarningOff());
                }, 2000);
            });
    };
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    const renderModal = () => {
        return (
            <Modal open={open} onCancel={handleCancel} closable={false} footer={null} className="top-1">
                <Typography.Title className="text-center">THÔNG TIN NGƯỜI DÙNG</Typography.Title>
                <Form onFinish={onFinish} onFinishFailed={onFinishFailed} autoComplete="off">
                    <Form.Item
                        name="taiKhoan"
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng nhập tên tài khoản!',
                            },
                        ]}
                    >
                        <div className="flex items-center justify-center border border-gray-200 rounded">
                            <div className="w-10 h-[38px] flex items-center justify-center border-r bg-[#E9ECEF]">
                                <span>
                                    <UserIcon className="h-4 w-4" />
                                </span>
                            </div>
                            <input
                                type="text"
                                placeholder="Tài khoản"
                                className="flex-grow px-3 py-[6px] border rounded-r focus-visible:outline-none focus:border-red-200"
                            />
                        </div>
                    </Form.Item>

                    <Form.Item
                        name="hoTen"
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng nhập họ và tên!',
                            },
                        ]}
                    >
                        <div className="flex items-center justify-center border border-gray-200 rounded">
                            <div className="w-10 h-[38px] flex items-center justify-center border-r bg-[#E9ECEF]">
                                <span>
                                    <BookIcon className="h-4 w-4" />
                                </span>
                            </div>
                            <input
                                type="text"
                                placeholder="Họ và tên"
                                className="flex-grow px-3 py-[6px] border rounded-r focus-visible:outline-none focus:border-red-200"
                            />
                        </div>
                    </Form.Item>

                    <Form.Item
                        name="email"
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng nhập email!',
                            },
                        ]}
                    >
                        <div className="flex items-center justify-center border border-gray-200 rounded">
                            <div className="w-10 h-[38px] flex items-center justify-center border-r bg-[#E9ECEF]">
                                <span>
                                    <EmailIcon className="h-4 w-4" />
                                </span>
                            </div>
                            <input
                                type="text"
                                placeholder="Email"
                                className="flex-grow px-3 py-[6px] border rounded-r focus-visible:outline-none focus:border-red-200"
                            />
                        </div>
                    </Form.Item>

                    <Form.Item
                        name="matKhau"
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng nhập password!',
                            },
                        ]}
                    >
                        <div className="flex items-center justify-center border border-gray-200 rounded">
                            <div className="w-10 h-[38px] flex items-center justify-center border-r bg-[#E9ECEF]">
                                <span>
                                    <KeyIcon className="h-4 w-4" />
                                </span>
                            </div>
                            <input
                                type="text"
                                placeholder="Password"
                                className="flex-grow px-3 py-[6px] border rounded-r focus-visible:outline-none focus:border-red-200"
                            />
                        </div>
                    </Form.Item>

                    <Form.Item
                        name="soDT"
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng nhập số điện thoại!',
                            },
                        ]}
                    >
                        <div className="flex items-center justify-center border border-gray-200 rounded">
                            <div className="w-10 h-[38px] flex items-center justify-center border-r bg-[#E9ECEF]">
                                <span>
                                    <PhoneIcon className="h-4 w-4" />
                                </span>
                            </div>
                            <input
                                type="text"
                                placeholder="Số điện thoại"
                                className="flex-grow px-3 py-[6px] border rounded-r focus-visible:outline-none focus:border-red-200"
                            />
                        </div>
                    </Form.Item>

                    <Form.Item
                        name="maLoaiNguoiDung"
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng nhập loại người dùng!',
                            },
                        ]}
                    >
                        <div className="flex items-center justify-center border border-gray-200 rounded">
                            <div className="w-10 h-[38px] flex items-center justify-center border-r bg-[#E9ECEF]">
                                <span>
                                    <CardIcon className="h-4 w-4" />
                                </span>
                            </div>
                            <select className="flex-grow px-3 py-[6px]  rounded-r focus-visible:outline-none focus:border-red-200">
                                <option value="">Loại người dùng</option>
                                <option value="GV">Giáo vụ</option>
                                <option value="HV">Học viên</option>
                            </select>
                        </div>
                    </Form.Item>
                    <Divider className="border border-gray-300" />

                    <div className="text-right">
                        <button
                            htmltype="submit"
                            className="px-3 py-[6px] rounded bg-[#41B294] text-white hover:bg-red-400"
                        >
                            Thêm người dùng
                        </button>
                        <button
                            className="px-3 py-[6px] rounded bg-red-700 text-white ml-2 hover:bg-red-800"
                            onClick={handleCancel}
                        >
                            Đóng
                        </button>
                    </div>
                </Form>
            </Modal>
        );
    };
    return <>{renderModal()}</>;
}
