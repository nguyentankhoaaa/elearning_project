import { https } from './config';

export const userServ = {
    postLogin: (loginForm) => {
        return https.post('/api/QuanLyNguoiDung/DangNhap', loginForm);
    },
    postRegister: (registerForm) => {
        return https.post('/api/QuanLyNguoiDung/DangKy', { ...registerForm, maNhom: 'GP01' });
    },
    getTeacherList: () => {
        return https.get('/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP01&tuKhoa=GV');
    },
    getAccountInfo: (account) => {
        return https.post('/api/QuanLyNguoiDung/ThongTinTaiKhoan', account);
    },
};
