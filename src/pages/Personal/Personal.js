import { Progress, Tabs } from 'antd';
import React, { useEffect, useState } from 'react';
import images from '../../assets/images/imgs';
import { localServ } from '../../services/localService';
import { userServ } from '../../services/userService';
import { ChartIcon } from '../../components/Icons/Icons';

export default function Personal() {
    const [accountInfo, setAccountInfo] = useState([]);
    const userInfo = { taiKhoan: localServ.get()?.taiKhoan };
    useEffect(() => {
        userServ
            .getAccountInfo(userInfo)
            .then((res) => {
                console.log(res);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);
    const onChange = (key) => {
        console.log(key);
    };

    const renderUserInfo = () => {
        return (
            <div>
                <div>
                    <div className="grid grid-cols-2 justify-between items-center">
                        <span>Email: {localServ.get().email}</span>
                        <span>Họ và tên: {localServ.get().hoTen}</span>
                        <span className="my-4">Số điện thoại: {localServ.get().soDT}</span>
                        <span className="my-4">Tài khoản: {localServ.get()?.taiKhoan}</span>
                        <span>Mã nhóm: {localServ.get().maNhom}</span>
                        <span>Đối tượng: {localServ.get().maLoaiNguoiDung}</span>
                    </div>
                    <div className="text-right mt-3">
                        <button className="px-3 py-2 bg-[#F6BA35] text-white rounded-lg hover:bg-green-500">
                            Cập nhật
                        </button>
                    </div>
                </div>
                <div>
                    <h2 className="text-3xl font-medium mb-6">Kỹ năng của tôi</h2>
                    <div class="flex">
                        <di className="flex-grow">
                            <div className="flex items-center mb-4">
                                <span className="w-16 h-10 flex items-center justify-center bg-[#F9CA9A] text-white rounded-md mr-6">
                                    HTML
                                </span>
                                <Progress
                                    percent={100}
                                    status="normal"
                                    showInfo={false}
                                    strokeColor={{
                                        from: '#108ee9',
                                        to: '#87d068',
                                    }}
                                />
                            </div>
                            <div className="flex items-center mb-4">
                                <span className="w-16 h-10 flex items-center justify-center bg-[#F9CA9A] text-white rounded-md mr-6">
                                    React
                                </span>
                                <Progress
                                    percent={70}
                                    status="normal"
                                    showInfo={false}
                                    strokeColor={{
                                        from: '#108ee9',
                                        to: '#87d068',
                                    }}
                                />
                            </div>
                            <div className="flex items-center mb-4">
                                <span className="w-16 h-10 flex items-center justify-center bg-[#F9CA9A] text-white rounded-md mr-6">
                                    CSS
                                </span>
                                <Progress
                                    percent={90}
                                    status="normal"
                                    showInfo={false}
                                    strokeColor={{
                                        from: '#108ee9',
                                        to: '#87d068',
                                    }}
                                />
                            </div>
                            <div className="flex items-center mb-4">
                                <span className="w-16 h-10 flex items-center justify-center bg-[#F9CA9A] text-white rounded-md mr-6">
                                    JS
                                </span>
                                <Progress
                                    percent={800}
                                    status="normal"
                                    showInfo={false}
                                    strokeColor={{
                                        from: '#108ee9',
                                        to: '#87d068',
                                    }}
                                />
                            </div>
                        </di>
                        <div className="grid grid-cols-2 gap-2">
                            <div className="flex items-center justify-center w-[120px] h-[120px] bg-[#41B294] rounded-lg">
                                <ChartIcon />
                                <div className="flex flex-col ml-2 text-white">
                                    <span className="text-lg">Giờ học</span>
                                    <span className="text-lg">80</span>
                                </div>
                            </div>
                            <div className="flex items-center justify-center w-[120px] h-[120px] bg-[#41B294] rounded-lg">
                                <ChartIcon />
                                <div className="flex flex-col ml-2 text-white">
                                    <span className="text-lg">Điểm tổng</span>
                                    <span className="text-lg">80</span>
                                </div>
                            </div>
                            <div className="flex items-center justify-center w-[120px] h-[120px] bg-[#41B294] rounded-lg">
                                <ChartIcon />
                                <div className="flex flex-col ml-2 text-white">
                                    <span className="text-lg">Buổi học</span>
                                    <span className="text-lg">40</span>
                                </div>
                            </div>
                            <div className="flex items-center justify-center w-[120px] h-[120px] bg-[#41B294] rounded-lg">
                                <ChartIcon />
                                <div className="flex flex-col ml-2 text-white">
                                    <span className="text-lg">Cấp độ</span>
                                    <span className="text-lg">Senior</span>
                                </div>
                            </div>
                            <div className="flex items-center justify-center w-[120px] h-[120px] bg-[#41B294] rounded-lg">
                                <ChartIcon />
                                <div className="flex flex-col ml-2 text-white">
                                    <span className="text-lg">Học lực</span>
                                    <span className="text-lg">Giỏi</span>
                                </div>
                            </div>
                            <div className="flex items-center justify-center w-[120px] h-[120px] bg-[#41B294] rounded-lg">
                                <ChartIcon />
                                <div className="flex flex-col ml-2 text-white">
                                    <span className="text-lg">Bài tập</span>
                                    <span className="text-lg">80</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    };

    const items = [
        {
            key: '1',
            label: `Thông tin cá nhân`,
            children: renderUserInfo(),
        },
        {
            key: '2',
            label: `Khóa học`,
            children: `Hiện chưa có khóa học mới.`,
        },
    ];
    return (
        <>
            <div className="p-[50px] bg-[#FFD60A] text-white uppercase">
                <h2 className="text-3xl">Thông tin cá nhân</h2>
                <h4>Thông tin học viên</h4>
            </div>
            <div className="flex flex-col px-20 py-10 mr-10">
                <div className="flex flex-col items-center justify-start mb-4">
                    <img src={images.user} alt="" className="h-28 w-28" />
                    <span className="my-3 font-semibold">Vincent</span>
                    <span>Lập trình viên Wed FullStack</span>
                    <button className="px-3 py-2 mt-3 bg-[#41B294] text-white rounded-full hover:bg-red-400">
                        Hồ sơ cá nhân
                    </button>
                </div>
                <div className="flex-grow ml-20">
                    <Tabs defaultActiveKey="1" items={items} onChange={onChange} />
                </div>
            </div>
        </>
    );
}
