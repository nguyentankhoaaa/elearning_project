import { userServ } from '../../services/userService';
import { NavLink, Navigate, useNavigate } from 'react-router-dom';
import { setSuccessOff, setSuccessOn } from '../../react-toolkit/successNotifySlice';
import { useDispatch, useSelector } from 'react-redux';
import { setLoadingOff, setLoadingOn } from '../../react-toolkit/loadingSlice';
import { setWarningOff, setWarningOn } from '../../react-toolkit/warningNotifySlice';
import { Button, Form, Input, Select, message } from 'antd';
import WarningNotify from '../../components/WarningNotify/WarningNotify';
import { setUserWarningOff, setUserWarningOn } from '../../react-toolkit/UserWarningNotifySlice ';

const RegisterPage = () => {
    let navigate = useNavigate();

    let dispatch = useDispatch();
    const tailFormItemLayout = {
        wrapperCol: {
            xs: {
                span: 24,
                offset: 8,
            },
            sm: {
                span: 16,
                offset: 8,
            },
        },
    };
    const [form] = Form.useForm();
    const onFinish = (values) => {
        userServ
            .postRegister(values)
            .then((res) => {
                dispatch(setSuccessOn('Register success'));
                setTimeout(() => {
                    dispatch(setSuccessOff());
                    navigate('/login');
                }, 2000);
            })
            .catch((err) => {
                console.log(err);
                dispatch(setUserWarningOn());
                setTimeout(() => {
                    dispatch(setUserWarningOff());
                }, 2000);
            });
    };
    return (
        <div className=" z-50 p-36 pb-16 pt-10 px-80 form-2 flex justify-center items-center ">
            <h2 className="text-signUp absolute top-14 z-40 text-gray-600 text-5xl font-serif">Sign Up</h2>
            <Form
                className=" w-3/4 py-10   px-28 form-register shadow-xl rounded-xl"
                layout="vertical"
                form={form}
                name="register"
                onFinish={onFinish}
                initialValues={{
                    residence: ['zhejiang', 'hangzhou', 'xihu'],
                    prefix: '96',
                }}
                scrollToFirstError
            >
                <Form.Item
                    classname="font-medium text-gray-600"
                    className="font-medium text-gray-600"
                    name="taiKhoan"
                    label="Tài Khoản"
                    tooltip="What do you want others to call you?"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your nickname!',
                            whitespace: true,
                        },
                    ]}
                >
                    <Input placeholder="Enter Username" />
                </Form.Item>
                <Form.Item
                    className="font-medium text-gray-600"
                    name="hoTen"
                    label="Tên Người Dùng "
                    rules={[
                        {
                            type: 'string',
                            message: 'The input is not valid E-mail!',
                        },
                        {
                            required: true,
                            message: 'Please input your name!',
                        },
                    ]}
                >
                    <Input placeholder="Enter User" />
                </Form.Item>
                <Form.Item
                    className="font-medium text-gray-600"
                    name="email"
                    label="E-mail"
                    rules={[
                        {
                            type: 'email',
                            message: 'The input is not valid E-mail!',
                        },
                        {
                            required: true,
                            message: 'Please input your E-mail!',
                        },
                    ]}
                >
                    <Input placeholder="Enter Email" />
                </Form.Item>
                <Form.Item
                    className="font-medium text-gray-600"
                    name="matKhau"
                    label="Mật Khẩu"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your password!',
                        },
                    ]}
                    hasFeedback
                >
                    <Input.Password placeholder="Enter Password" />
                </Form.Item>
                <Form.Item
                    className="font-medium text-gray-600"
                    name="confirm"
                    label="Nhập Lại Mật Khẩu"
                    dependencies={['matKhau']}
                    hasFeedback
                    rules={[
                        {
                            required: true,
                            message: 'Please confirm your password!',
                        },
                        ({ getFieldValue }) => ({
                            validator(_, value) {
                                if (!value || getFieldValue('matKhau') === value) {
                                    return Promise.resolve();
                                }
                                return Promise.reject(new Error('The two passwords that you entered do not match!'));
                            },
                        }),
                    ]}
                >
                    <Input.Password placeholder="Enter Password Again" />
                </Form.Item>

                <Form.Item className="font-medium text-gray-600 text-right" {...tailFormItemLayout}>
                    <Button htmlType="submit" className="button border-1 font-medium border-gray-500 ">
                        Đăng ký
                    </Button>
                </Form.Item>
                <NavLink to="/login" className="text-base text-right">
                    <p className="text-base">Bạn đã có tài khoản ? Đăng Nhập</p>
                </NavLink>
            </Form>
        </div>
    );
};
export default RegisterPage;
