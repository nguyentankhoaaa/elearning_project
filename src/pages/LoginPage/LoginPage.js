import { Button, Checkbox, Form, Input, message } from 'antd';
import { useEffect } from 'react';
import { userServ } from '../../services/userService';
import { NavLink, Navigate, useNavigate } from 'react-router-dom';
import { setSuccessOff, setSuccessOn } from '../../react-toolkit/successNotifySlice';
import { useDispatch } from 'react-redux';
import { localServ } from '../../services/localService';
import { userInfor } from '../../react-toolkit/userLoginSlice';
import { setLoginWarningOff, setLoginWarningOn } from '../../react-toolkit/LoginWarningNotifySlice';

const LoginPage = () => {
    let navigate = useNavigate();
    let dispatch = useDispatch();

    const onFinish = (values) => {
        userServ
            .postLogin(values)
            .then((res) => {
                localServ.set(res.data);
                setTimeout(() => {
                    navigate('/home');
                    dispatch(setSuccessOff());
                }, 2000);
                dispatch(setSuccessOn('Login success'));
                dispatch(userInfor(res.data));
            })
            .catch((err) => {
                console.log(err);
                dispatch(setLoginWarningOn());
                setTimeout(() => {
                    dispatch(setLoginWarningOff());
                }, 2000);
            });
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    return (
        <div className="py-48 px-80 form-1 pb-36 flex justify-center items-center h-screen">
            <h2 className="text-signIn absolute top-36 z-40 text-gray-600 text-5xl font-serif">Sign In</h2>
            <Form
                layout="vertical"
                className="   px-36 form-login shadow-xl rounded-xl"
                name="basic"
                labelCol={{
                    span: 8,
                }}
                wrapperCol={{
                    span: 24,
                }}
                style={{
                    width: '100%',
                }}
                initialValues={{
                    remember: true,
                }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
            >
                <Form.Item
                    className="font-medium text-gray-600"
                    label="Tài Khoản"
                    name="taiKhoan"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your username!',
                        },
                    ]}
                >
                    <Input placeholder="Enter Username" />
                </Form.Item>

                <Form.Item
                    className="font-medium text-gray-600"
                    label="Mật Khẩu"
                    name="matKhau"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your password!',
                        },
                    ]}
                >
                    <Input.Password placeholder="Enter Password" />
                </Form.Item>

                <Form.Item
                    className="text-right "
                    wrapperCol={{
                        span: 24,
                    }}
                >
                    <Button className="border-1 font-medium border-gray-500" htmlType="submit">
                        Đăng Nhập
                    </Button>
                </Form.Item>
                <Form.Item className="text-base text-right">
                    <NavLink to="/register">Bạn chưa có tài khản ? Đăng Ký</NavLink>
                </Form.Item>
            </Form>
        </div>
    );
};
export default LoginPage;
