import InstructorPage from './InstructorPage/InstructorPage';
import ListCourse from './ListCoursePage/ListCourse';
import NumberBox from './NumberBox/NumberBox';
import ReviewPage from './ReviewPage/ReviewPage';

export default function HomePage() {
    return (
        <div className="space-y-10 pl-10 pr-10">
            <ListCourse className="mt-[86px]" />
            <NumberBox />
            <InstructorPage />
            <ReviewPage />
        </div>
    );
}
