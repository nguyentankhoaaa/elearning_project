import React from 'react'

export default function Footer() {
  return (
  <div className='footer' style={{backgroundColor:"#f0f8ff"}}>
      <div className='colums columns-4 mt-10 px-16 pt-9 pb-5' >
      <ul className='col-1 font-medium '>
        <li className='relative mb-4'
         style={{textShadow:"5px -2px 3px #5AB1BB"}} >
          <i class="fab fa-etsy text-cyan-600 text-4xl font-bold "></i>
         <span  className='font-bold text-gray-600 text-2xl'>
          learning
        </span>
        <span className='absolute left-24 bottom-4 text-xl'><i className="fa fa-keyboard " />
</span>
</li>

  <li className='mt-3' ><i className="fa fa-phone text-white p-3 mr-2 rounded-3xl bg-cyan-600 " /> <span>1800-123-4567</span></li>
  <li className='mt-3' ><i className="fa fa-envelope text-white p-3 mr-2 rounded-3xl bg-cyan-600" /> <span>E-learning@gmail.com</span></li>
  <li className='mt-3' ><i className="fa fa-map-marker-alt text-white p-3 mr-2 rounded-3xl bg-cyan-600" /> <span>Gia lai</span></li>
      </ul>
      <ul className='col-2 font-medium lienKet'>
        <li className='text-2xl font-bold'>Liên Kết</li>
        <li  className='mt-3 li'><i className='fas fa-chevron-right text-xs'></i> Trang chủ</li>
        <li  className='mt-3 li'><i className='fas fa-chevron-right text-xs'></i> Dịch vụ</li>
        <li  className='mt-3 li'><i className='fas fa-chevron-right text-xs'></i> Nhóm</li>
        <li  className='mt-3 li'><i className='fas fa-chevron-right text-xs'></i> Blog</li>
      </ul>
      <ul className='col-3 font-medium khoaHoc'>
        <li className='text-2xl font-bold'>Khóa Học</li>
        <li  className='mt-3 li'><i className='fas fa-chevron-right text-xs'></i> Front End</li>
        <li  className='mt-3 li'><i className='fas fa-chevron-right text-xs'></i> Back End</li>
        <li  className='mt-3 li'><i className='fas fa-chevron-right text-xs'></i> Full Stack</li>
        <li  className='mt-3 li'><i className='fas fa-chevron-right text-xs'></i> Node Js</li>
      </ul>
      <ul className='col-4 font-medium tuVan'>
        <li className='text-2xl font-bold'>Đăng Kí Tư Vấn</li>
      <li  className='mt-3 li'><input  style={{border:"2px solid #5AB1BB",}} className='input rounded-xl px-2 py-2 ' type="text" placeholder='Họ và tên' /></li>
      <li  className='mt-1 li'><input  style={{border:"2px solid #5AB1BB",}} className='input rounded-xl px-2 py-2 ' type="email" placeholder='Email' /></li>
    <li  className='mt-1 li'><input  style={{border:"2px solid #5AB1BB",}} className='input rounded-xl px-2 py-2 ' type="text" placeholder='Số Điện Thoại' /></li>
    <li><button type='submit' className='bg-cyan-600 text-white py-1 px-2  mt-2 rounded-lg'>Đăng Kí</button></li>
       
      </ul>
    </div>
  
   <div className='px-16 py-4 flex footer-end justify-between' style={{borderTop:"1px solid #d0d0d0"}}>
    <h2 className='font-medium'>Copyright © 2021. All rights reserved.</h2>
    <div>
    <div>
  <i className="fab fa-instagram text-white bg-cyan-600 py-2 font-medium px-2 ml-2 rounded-2xl " />
  <i className="fab fa-facebook text-white bg-cyan-600 py-2 font-medium px-2 ml-2 rounded-2xl " />
  <i className="fab fa-twitter text-white bg-cyan-600 py-2 font-medium px-2 ml-2 rounded-2xl " />
</div>

    </div>

   </div>
  </div>
  )
}
