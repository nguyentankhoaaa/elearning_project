import { createSlice } from '@reduxjs/toolkit';
import { localServ } from '../services/localService';

const initialState = {
    userInfo: localServ.get(),
    updateForm: null,
};

const adminUserSlice = createSlice({
    name: 'adminUserSlice',
    initialState,
    reducers: {
        setLogin: (state, action) => {
            state.userInfo = action.payload;
        },

        setFillUpdateForm: (state, action) => {
            state.updateForm = localServ?.get();
        },

        setEmptyUpdateForm: (state, action) => {
            state.updateForm = null;
        },
    },
});

export const { setLogin, setFillUpdateForm, setEmptyUpdateForm } = adminUserSlice.actions;

export default adminUserSlice.reducer;
