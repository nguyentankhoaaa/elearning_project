import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router';
import { BookIcon } from '../../components/Icons/Icons';
import { courceServ } from '../../services/courseService';
import ItemCourseList from './ItemCourseList';

export default function CourseListFollowCategory() {
    let { courses } = useParams();
    const [listCourse, setListCourse] = useState([]);

    useEffect(() => {
        courceServ
            .getCourseListFollowCategory(courses)
            .then((res) => {
                setListCourse(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, [courses]);
    return (
        <>
            <div className="p-[50px] bg-[#FFD60A] text-white uppercase">
                <h2 className="text-3xl">Khóa học theo danh mục</h2>
                <h4>Hãy chọn khóa học mong muốn!!!</h4>
            </div>

            <div className="px-10 py-12">
                <span className="flex items-center px-2 py-3 rounded-full border-2 border-[#e4e4e4] w-fit">
                    <BookIcon className="mr-2" />
                    Lập trình {courses}
                </span>
            </div>
            <div className="px-10 grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-1 gap-4 justify-between items-center">
                {listCourse.map((item, index) => {
                    return <ItemCourseList key={index} item={item} />;
                })}
            </div>
        </>
    );
}
