import React from 'react'
import education  from "../../assets/img/infor_img/education-hero.png"
import student  from "../../assets/img/infor_img/students.png"
import study  from "../../assets/img/infor_img/olstudy.png"
import hero_flex  from "../../assets/img/infor_img/hero-flex.png"
export default function InforPage() {
  return (
    <div className='inforPage'>
<div className='banner py-48'>
<div className='content-banner text-center'>
<p className='text-green-500 font-medium text-xl'>V LEARNING HỌC LÀ VUI</p>
<p className='text-4xl text-yellow-500 font-bold py-3'>Cùng nhau khám phá nhưng điều mới mẻ</p>
<p className='text-white text-xl font-medium'>Học đi đôi với hành</p>
</div>
</div>
<section className='container px-16 py-10 columns-2'>
<div className='item-content'>
<p className='text-xl font-bold text-green-600'>V LEARNING ?</p>
<p className='text-4xl py-4 font-medium'>Nơi hội tụ kiến thức</p>
<p className='title'>Đây là nền tảng giảng dạy và học tập trực tuyến được xây dựng để hỗ trợ phát triển giáo dục trong thời đại công nghiệp 4.0, được xây dựng dựa trên cơ sở quan sát toàn bộ các nhu cầu cần thiết của một lớp học offline. Từ đó đáp ứng và đảm bảo cung cấp các công cụ toàn diện, dễ sử dụng cho giáo viên và học sinh, 
    giúp tạo nên một lớp học trực tuyến thú vị và hấp dẫn.</p>
</div>
<div className="item-img blue">
    <img src={education} alt="" />
</div>
<div className='item-content'>
<p className='text-xl font-bold text-green-600'>TẦM NHÌN V LEARNING</p>
<p className='text-4xl py-4 font-medium'>Đào tạo lập trình chuyên sâu</p>
<p className='title'>Trở thành hệ thống đào tạo lập trình chuyên sâu theo nghề hàng đầu khu vực, cung cấp nhân lực có tay nghề cao, chuyên môn sâu cho sự phát triển công nghiệp phần mềm trong thời đại công nghệ số hiện nay, góp phần giúp sự phát triển của xã hội, đưa Việt Nam thành cường quốc về phát triển phần mềm.giúp người học phát triển cả tư duy, phân tích, chuyên sâu nghề nghiệp, học tập suốt đời,
 sẵn sàng đáp ứng mọi nhu cầu của doanh nghiệp.</p>
</div>
<div className="item-img blue">
    <img src={student} alt="" />
</div>
<div className="item-img green">
    <img src={hero_flex} alt="" />
</div>
<div className='item-content'>
<p className='text-xl font-bold text-green-600'>
SỨ MỆNH V LEARNING</p>
<p className='text-4xl py-4 font-medium'>Phương pháp đào tạo hiện đại</p>
<p className='title'>Sử dụng các phương pháp đào tạo tiên tiến và hiện đại 
trên nền tảng công nghệ giáo dục, kết hợp phương pháp truyền thống, phương pháp t
rực tuyến, lớp học đảo ngược và học tập dựa trên dự án thực tế, phối hợp giữa đội ngũ 
training nhiều kinh nghiệm và yêu cầu từ các công ty, doanh nghiệp. Qua đó, V learning
 giúp người học phát triển cả tư duy, phân tích,chuyên sâu nghề nghiệp, học tập suốt đời, sẵn s
àng đáp ứng mọi nhu cầu của doanh nghiệp.</p>
</div>
<div className="item-img green">
    <img src={study} alt="" />
</div>
<div className='item-content'>
<p className='text-xl font-bold text-green-600'>
CHƯƠNG TRÌNH HỌC V LEARNING</p>
<p className='text-4xl py-4 font-medium'>Hệ thống học hàng đầu</p>
<p className='title'>Giảng viên đều là các chuyên viên thiết kế đồ họa và 
giảng viên của các trường đại học danh tiếng trong thành phố: Đại học CNTT, 
KHTN, Bách Khoa,…Trang thiết bị phục vụ học tập đầy đủ, phòng học máy lạnh,
 màn hình LCD, máy cấu hình mạnh, mỗi học viên thực hành trên một máy riêng.
 100% các buổi học đều là thực hành trên máy tính. Đào tạo với tiêu chí: “học để làm được việc”
, mang lại cho học viên những kiến thức hữu ích nhất, sát với thực tế nhất.</p>
</div>
</section>
    </div>
  )
}
