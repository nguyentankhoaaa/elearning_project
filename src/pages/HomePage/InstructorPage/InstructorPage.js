import React from 'react';
import './InstructorPage.css';

export default function InstructorPage() {
    return (
        <div className="mt-5 instructorContainer">
            <h6>Giảng viên hàng đầu</h6>
            <input type="checkbox" id="sliderInstructors" hidden />
            <div className="instructorItem">
                <div class="grid grid-flow-col mt-4 gap-4">
                    <div className="sm:col-span-4 md:col-span-4 lg:col-span-4 xl:col-span-1">
                        <div className="instructorContent">
                            <img src="./image/instrutor5.2e4bd1e6.jpg" alt="" />
                            <h6>Bladin Slaham</h6>
                            <div className="textReviewRole">
                                <p>Chuyên gia lĩnh vực</p>
                                <p>lập trình</p>
                            </div>
                            <p className="reviewMentor">
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <span className="textStar">4.9</span>
                            </p>
                            <span className="textReviewBot">100 Đánh giá</span>
                        </div>
                    </div>
                    <div className="sm:col-span-4 md:col-span-4 lg:col-span-4 xl:col-span-1">
                        <div className="instructorContent">
                            <img src="./image/instrutor6.64041dca.jpg" alt="" />
                            <h6>Bladin Slaham</h6>
                            <div className="textReviewRole">
                                <p>Chuyên gia ngôn ngữ</p>
                                <p>Vue JS</p>
                            </div>
                            <p className="reviewMentor">
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <span className="textStar">4.9</span>
                            </p>
                            <span className="textReviewBot">100 Đánh giá</span>
                        </div>
                    </div>
                    <div className="sm:col-span-4 md:col-span-4 lg:col-span-4 xl:col-span-1">
                        <div className="instructorContent">
                            <img src="./image/instrutor7.edd00a03.jpg" alt="" />
                            <h6>Bladin Slaham</h6>
                            <div className="textReviewRole">
                                <p>Chuyên gia hệ thống</p>
                                <p>máy tính</p>
                            </div>
                            <p className="reviewMentor">
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <span className="textStar">4.9</span>
                            </p>
                            <span className="textReviewBot">100 Đánh giá</span>
                        </div>
                    </div>
                    <div className="sm:col-span-4 md:col-span-4 lg:col-span-4 xl:col-span-1">
                        <div className="instructorContent">
                            <img src="./image/instrutor8.aec2f526.jpg" alt="" />
                            <h6>Bladin Slaham</h6>
                            <div className="textReviewRole">
                                <p>Chuyên gia lĩnh vực</p>
                                <p>Full Skill</p>
                            </div>
                            <p className="reviewMentor">
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <span className="textStar">4.9</span>
                            </p>
                            <span className="textReviewBot">100 Đánh giá</span>
                        </div>
                    </div>
                    <div className="sm:col-span-4 md:col-span-4 lg:col-span-4 xl:col-span-1">
                        <div className="instructorContent">
                            <img src="./image/instrutor9.504ea6c5.jpg" alt="" />
                            <h6>Bladin Slaham</h6>
                            <div className="textReviewRole">
                                <p>Chuyên gia lĩnh vực</p>
                                <p>phân tích</p>
                            </div>
                            <p className="reviewMentor">
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <span className="textStar">4.9</span>
                            </p>
                            <span className="textReviewBot">100 Đánh giá</span>
                        </div>
                    </div>
                    <div className="sm:col-span-4 md:col-span-4 lg:col-span-4 xl:col-span-1">
                        <div className="instructorContent">
                            <img src="./image/instrutor10.89946c43.jpg" alt="" />
                            <h6>Bladin Slaham</h6>
                            <div className="textReviewRole">
                                <p>Chuyên gia lĩnh vực</p>
                                <p>PHP</p>
                            </div>
                            <p className="reviewMentor">
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <span className="textStar">4.9</span>
                            </p>
                            <span className="textReviewBot">100 Đánh giá</span>
                        </div>
                    </div>
                    <div className="sm:col-span-4 md:col-span-4 lg:col-span-4 xl:col-span-1">
                        <div className="instructorContent">
                            <img src="./image/instrutor11.0387fe65.jpg" alt="" />
                            <h6>David Ngô Savani</h6>
                            <div className="textReviewRole">
                                <p>Chuyên gia lĩnh vực</p>
                                <p>Front End</p>
                            </div>
                            <p className="reviewMentor">
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <i className="fas fa-star"></i>
                                <span className="textStar">4.9</span>
                            </p>
                            <span className="textReviewBot">100 Đánh giá</span>
                        </div>
                    </div>
                </div>
            </div>
            <div className="sliderDot">
                <div className="dotLeft">
                    <label for="sliderInstructors" className="labelDotLeft"></label>
                    <div className="layDotLeft"></div>
                </div>
                <div className="dotRight">
                    <label for="sliderInstructors" className="labelDotRight"></label>
                    <div className="layDotRight"></div>
                </div>
            </div>
        </div>
    );
}
