import React from 'react';
import { useSelector } from 'react-redux';
import Lottie from 'lottie-react';
import warningNotify from '../../assets/lottie/warningNotify.json';

export default function WarningNotify() {
    const { isWarning, messageWarning } = useSelector((state) => state.warningNotifySlice);

    return isWarning ? (
        <div className="flex items-center justify-center h-screen w-screen left-0 top-0 z-[9999] fixed bg-[#9994]">
            <div className="flex items-center justify-center h-screen w-screen left-0 top-0 fixed bg-[#9994] z-[9999]">
                <div className="box bg-white rounded-md h-[200px] w-[400px] flex flex-col items-center justify-between">
                    <div className="title h-24 w-24 py-5">
                        <Lottie animationData={warningNotify} />
                    </div>
                    <div className="content py-5 text-[27px] font-semibold text-[#41b294]">
                        <span>{messageWarning}!</span>
                    </div>
                </div>
            </div>
        </div>
    ) : (
        <></>
    );
}
