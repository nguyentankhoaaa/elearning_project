import { Avatar, Card } from 'antd';
import './ItemCourseRef.css';
const { Meta } = Card;
const ItemCourseRef = ({ data }) => (
    <div className="cardEffect mt-4 cardGlobalRes">
        <a href={`/detail/${data.maKhoaHoc}`} className="cardGlobal moveSubCard">
            <img src={data.hinhAnh} alt="" />
            <span className="stickerCard">{data.tenKhoaHoc}</span>
            <div className="cardBodyGlobal">
                <h6>Lập trình hiện đang là xu hướng trên toàn thế giới...</h6>
                <div className="cardIcon">
                    <span>
                        <i class="far fa-clock iconOclock"></i> 8 giờ
                    </span>
                    <span>
                        <i class="far fa-calendar-alt iconCalendar"></i> 4 tuần
                    </span>
                    <span>
                        <i class="fas fa-signal iconLevel"></i> Tất cả
                    </span>
                </div>
            </div>
            <div className="cardFooter">
                <div className="titleMaker">
                    <div className="imgCardFooter">
                        <img src="./image/avatar2.bb9626e2.png" alt="" />
                    </div>
                    <span class="ml-2 colorCardTitle">Elon Musk</span>
                </div>
                <div>
                    <p>
                        800.000
                        <sup>đ</sup>
                    </p>
                    <p>
                        400.000
                        <sup>đ</sup>
                        <i class="fas fa-tag iconTag"></i>
                    </p>
                </div>
            </div>
            <div className="subCard">
                <div className="subCardHead">
                    <img src="./image/emoji.6d1b7051.png" alt="" />
                    <span className="ml-1 colorCardTitle">Elon Musk Richard</span>
                </div>
                <h6>BOOTCAMP - LẬP TRÌNH FULL STACK TỪ ZERO ĐẾN CÓ VIỆC</h6>
                <p class="colorCardTitle">
                    Đã có hơn 6200 bạn đăng kí học và có việc làm thông qua chương trình đào tạo Bootcamp Lập trình
                    Front End chuyên nghiệp. Khóa học 100% thực hành cường độ cao theo dự án thực tế và kết nối doanh
                    nghiệp hỗ trợ tìm việc ngay sau khi học...
                </p>
                <div className="cardIcon">
                    <span>
                        <i class="far fa-clock iconOclock"></i> 8 giờ
                    </span>
                    <span>
                        <i class="far fa-calendar-alt iconCalendar"></i> 4 tuần
                    </span>
                    <span>
                        <i class="fas fa-signal iconLevel"></i> Tất cả
                    </span>
                </div>
                <button class="btnGlobal btnSubCard">
                    <a href="/chitiet/aJava">Xem chi tiết</a>
                </button>
            </div>
            <div className="cardSale">
                <span>Yêu thích</span>
            </div>
        </a>
    </div>
);
export default ItemCourseRef;
