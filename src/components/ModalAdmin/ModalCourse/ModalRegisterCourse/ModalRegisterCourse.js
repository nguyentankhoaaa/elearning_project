import { Divider, Modal, Space, Table } from 'antd';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { setSuccessOff, setSuccessOn } from '../../../../react-toolkit/successNotifySlice';
import { adminServices } from '../../../../services/adminServices';
import SearchUsers from '../../../Search/SearchUsers';
import { approvedStudent, waitingStudent } from './utils';

export default function ModalRegisterCourse({ open, handleCancel, maKhoaHoc }) {
    const [waitingStudentList, setWaitingStudentList] = useState([]);
    const [approvedStudentList, setApprovedStudentList] = useState([]);
    const dispatch = useDispatch();
    const courseInfo = { maKhoaHoc };
    const fetchApi = () => {
        adminServices
            .getWaitingRegisterStudentList(courseInfo)
            .then((res) => {
                console.log(res);
                let newValues = res.data.map((item, index) => {
                    return {
                        ...item,
                        stt: index + 1,
                        action: (
                            <Space>
                                <button
                                    onClick={() => {
                                        handleAuthCourse({ maKhoaHoc, taiKhoan: item.taiKhoan });
                                    }}
                                    className="px-3 py-[6px] bg-[#41B294] rounded text-white hover:bg-red-400"
                                >
                                    Xác thực
                                </button>
                                <button
                                    onClick={() => {
                                        handleCancelRegister({
                                            maKhoaHoc,
                                            taiKhoan: item.taiKhoan,
                                        });
                                    }}
                                    className="px-3 py-[6px] bg-red-800 rounded text-white hover:bg-red-700"
                                >
                                    Xóa
                                </button>
                            </Space>
                        ),
                    };
                });

                setWaitingStudentList(newValues);
            })
            .catch((err) => {
                console.log(err);
            });

        adminServices
            .getRegisteredStudentList(courseInfo)
            .then((res) => {
                let newValue = res.data?.map((item, index) => {
                    return {
                        ...item,
                        stt: index + 1,
                        action: (
                            <Space>
                                <button
                                    onClick={() => {
                                        handleCancelRegister({
                                            maKhoaHoc,
                                            taiKhoan: item.taiKhoan,
                                        });
                                    }}
                                    className="px-3 py-[6px] bg-red-800 rounded text-white hover:bg-red-700"
                                >
                                    Hủy
                                </button>
                            </Space>
                        ),
                    };
                });
                setApprovedStudentList(newValue);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    const handleCancelRegister = (dataCourse) => {
        adminServices
            .cancelRegister(dataCourse)
            .then((res) => {
                dispatch(setSuccessOn('Hủy thành công'));
                setTimeout(() => {
                    dispatch(setSuccessOff());
                }, 2000);
                fetchApi();
            })
            .catch((err) => {
                console.log(err);
            });
    };

    const handleAuthCourse = (data) => {
        adminServices
            .registerCourse(data)
            .then((res) => {
                dispatch(setSuccessOn('Ghi danh thành công'));
                setTimeout(() => {
                    dispatch(setSuccessOff());
                }, 2000);
                fetchApi();
            })
            .catch((err) => {
                console.log(err);
            });
    };

    useEffect(() => {
        fetchApi();
    }, [maKhoaHoc]);

    return (
        <>
            <Modal
                width={800}
                height={400}
                open={open}
                onCancel={handleCancel}
                closable={false}
                footer={null}
                className="top-1"
            >
                <div>
                    <div className="flex justify-between items-center">
                        <span className="font-semibold text-xl">Chọn người dùng</span>
                        <SearchUsers maKhoaHoc={maKhoaHoc} fetchApi={fetchApi} />
                    </div>
                    <Divider style={{ borderColor: 'black', margin: '12px 0px' }} />
                    <div>
                        <span className="font-semibold text-xl">Học viên chờ xác thực</span>
                        <Table
                            className="mt-3"
                            size="small"
                            columns={waitingStudent}
                            dataSource={waitingStudentList}
                            bordered
                            pagination={{
                                defaultPageSize: 2,
                                showSizeChanger: false,

                                position: ['bottomCenter'],
                            }}
                        ></Table>
                    </div>
                    <Divider style={{ borderColor: 'black' }} />
                    <div>
                        <span className="font-semibold text-xl">Học viên đã ghi danh</span>
                        <Table
                            className="mt-3"
                            size="small"
                            columns={approvedStudent}
                            dataSource={approvedStudentList}
                            bordered
                            pagination={{
                                defaultPageSize: 2,
                                showSizeChanger: false,
                                position: ['bottomCenter'],
                            }}
                        ></Table>
                    </div>
                    <Divider style={{ borderColor: 'black' }} />
                    <div className="text-right">
                        <button
                            onClick={handleCancel}
                            className="px-3 py-[6px] rounded bg-red-700 text-white ml-2 hover:bg-red-800"
                        >
                            Đóng
                        </button>
                    </div>
                </div>
            </Modal>
        </>
    );
}
