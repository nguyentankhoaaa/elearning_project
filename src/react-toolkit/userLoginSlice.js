import { createSlice } from '@reduxjs/toolkit';
import { localServ } from '../services/localService';

const initialState = {
    inforUser: localServ.get(),
};
export const userLoginSlice = createSlice({
    name: 'userLoginSlice',
    initialState,
    reducers: {
        userInfor: (state, action) => {
            state.inforUser = action.payload;
        },
    },
});
export const { userInfor } = userLoginSlice.actions;
export default userLoginSlice.reducer;
