import axios from 'axios';
import { localServ } from './localService';

export const BASE_URL = 'https://elearningnew.cybersoft.edu.vn';
const TokenCybersoft =
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA1NiIsIkhldEhhblN0cmluZyI6IjAzLzA0LzIwMjQiLCJIZXRIYW5UaW1lIjoiMTcxMjEwMjQwMDAwMCIsIm5iZiI6MTY4MzMwNjAwMCwiZXhwIjoxNzEyMjUwMDAwfQ.YeDhc_oSixV2XtFPDzcpxFhBos5832JpQpndHNoqZLk';

export const configHeader = () => {
    return {
        TokenCybersoft: TokenCybersoft,
        Authorization: 'bearer ' + localServ.get()?.accessToken,
    };
};
export const https = axios.create({
    baseURL: BASE_URL,
    headers: configHeader(),
});
