import React from 'react'
import { NavLink } from 'react-router-dom'

export default function ItemDetailPage({item}) {
  return (
    <div className='course-container'>
           <NavLink  onClick="scroll(0,0)" to={`/detail/${item.maKhoaHoc}`}>
<div className=" item-course mt-3 rounded-sm bg-white relative  shadow-2xl ">
    <img className="rounded-xl  w-full h-48 object-contain" src={item.hinhAnh}  />
  <div className="p-5 ">

      <h5 style={{top:"169px"}} className=" text-base
      left-0  font-sans bg-green-600 px-1 rounded-r-lg tracking-tight text-white absolute">{item.tenKhoaHoc}</h5>
  
    <p className="mb-1 font-normal text-gray-700 dark:text-black">Chuyên ngành công nghệ phần mềm là gì ( Công nghệ t...</p>
 <div className='grid grid-cols-3'>
<div>
<p>  <i className='fa fa-clock text-orange-300'></i> <span>8 giờ</span></p>
</div>
<div>
<p>  <i className='fa fa-calendar text-red-400'></i> <span>4 tuần</span></p>
</div>
<div>
<p>  <i className='fa fa-signal text-blue-400'></i> <span>Tất cả</span></p>
</div>


 </div>
 
  <hr className='w-full mt-4 bg-gray-600 ' style={{height:"1px"}} />

  <div className='grid grid-cols-2  mt-2'>
 <div className='flex items-center  '>
  <i className="fa fa-user-circle text-red-500 text-2xl" />  
  <p className='ml-1 text-sm'>{item.nguoiTao.hoTen}</p>
  </div>

   <div>
   <div className='relative ml-10'>
      <p className='text-xs line-through text-gray-500'>800.000 <span className='absolute bottom-7'>đ</span></p>
      <p className='text-base font-medium  text-green-600'>500.000 <span className='absolute bottom-2'>đ</span> 
    <span className='absolute left-16 ml-2 bottom-1 text-xl '> <i className='fa fa-tag text-red-500'></i></span>
</p>

    </div>
   </div>
  </div>
  </div>
  <div className='cardSale'>
 <span className='text-white font-medium'>YÊU THÍCH</span>
  </div>
</div>
</NavLink>
    </div>
    )
  }
