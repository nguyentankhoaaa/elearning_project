import React, { useEffect, useState } from 'react';
import Tippy from '@tippyjs/react/headless';
import { adminServices } from '../../services/adminServices';
import { useDispatch } from 'react-redux';
import { setSuccessOff, setSuccessOn } from '../../react-toolkit/successNotifySlice';
import { setWarningOn, setWarningOff } from '../../react-toolkit/warningNotifySlice';

export default function SearchUsers({ maKhoaHoc, fetchApi }) {
    const [userList, setUserList] = useState([]);
    const [filterResult, setFilterResult] = useState([]);
    const [searchInput, setSearchInput] = useState('');
    const [registerCourse, setRegisterCourse] = useState({
        maKhoaHoc: maKhoaHoc,
        taiKhoan: null,
    });
    const [show, setShow] = useState(false);
    const dispatch = useDispatch();

    const courseInfo = { maKhoaHoc };

    const getDataAPI = () => {
        adminServices
            .getUnRegisterStudentList(courseInfo)
            .then((res) => {
                setUserList(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    useEffect(() => {
        getDataAPI();
    }, [maKhoaHoc]);

    const searchItems = (searchValue) => {
        setSearchInput(searchValue);

        if (searchInput != '') {
            const filterData = userList.filter((item) => {
                return Object.values(item.hoTen).join('').toLowerCase().includes(searchInput.toLowerCase());
            });
            setFilterResult(filterData);
        } else {
            setFilterResult(userList);
        }
    };

    const handleShow = () => {
        setShow(true);
    };

    const handleHide = () => {
        setShow(false);
    };

    const handleOnClick = (e) => {
        setSearchInput(e.target.innerText);
        setRegisterCourse({ maKhoaHoc: maKhoaHoc, taiKhoan: e.target.dataset.taikhoan });
        setShow(false);
    };

    const handleRegisterCourse = () => {
        adminServices
            .registerCourse(registerCourse)
            .then((res) => {
                dispatch(setSuccessOn('Đã ghi danh thành công'));
                setTimeout(() => {
                    dispatch(setSuccessOff());
                }, 2000);
                setSearchInput('');
                setRegisterCourse({ maKhoaHoc: maKhoaHoc, taiKhoan: null });
                getDataAPI();
                fetchApi();
            })
            .catch((err) => {
                dispatch(setWarningOn(err.response.data));
                setTimeout(() => {
                    dispatch(setWarningOff());
                }, 2000);
                console.log(err.response.data);
            });
    };
    return (
        <>
            <div>
                <Tippy
                    offset={(0, 0)}
                    placement={'bottom-start'}
                    interactive
                    visible={searchItems && show}
                    onClickOutside={handleHide}
                    render={(attrs) => (
                        <div
                            className="flex flex-col border border-[#ced4da] bg-white w-[300px] rounded`"
                            tabIndex="-1"
                            {...attrs}
                        >
                            <div className="max-h-80 overflow-scroll overflow-x-hidden">
                                {searchInput.length > 1
                                    ? filterResult.map((item) => {
                                          return (
                                              <div
                                                  onClick={handleOnClick}
                                                  key={item.taiKhoan}
                                                  className="hover:bg-[#ede6e6ce] px-3 py-2 hover:cursor-pointer"
                                              >
                                                  {item.hoTen}
                                              </div>
                                          );
                                      })
                                    : userList.map((item) => {
                                          return (
                                              <div
                                                  data-taikhoan={item.taiKhoan}
                                                  onClick={handleOnClick}
                                                  key={item.taiKhoan}
                                                  className="hover:bg-[#ede6e6ce] px-3 py-2 hover:cursor-pointer"
                                              >
                                                  {item.hoTen}
                                              </div>
                                          );
                                      })}
                            </div>
                        </div>
                    )}
                >
                    <input
                        value={searchInput}
                        onFocus={handleShow}
                        onChange={(e) => searchItems(e.target.value)}
                        className="w-[300px] focus-visible:outline-0 border border-[#ced4da] rounded px-3 py-[6px] focus:border-red-400"
                        type="text"
                        placeholder="Tên người dùng"
                    />
                </Tippy>
            </div>
            <button
                onClick={handleRegisterCourse}
                className="px-3 py-[6px] bg-[#41B294] rounded text-white hover:bg-red-400"
            >
                Ghi danh
            </button>
        </>
    );
}
