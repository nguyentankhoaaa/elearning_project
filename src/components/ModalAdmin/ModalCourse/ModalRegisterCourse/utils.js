export const waitingStudent = [
    {
        title: 'STT',
        dataIndex: 'stt',
    },
    {
        title: 'Tài khoản',
        dataIndex: 'taiKhoan',
        align: 'center',
    },
    {
        title: 'Học viên',
        dataIndex: 'hoTen',
        align: 'center',
    },
    {
        title: 'Chờ xác nhận',
        dataIndex: 'action',
        align: 'center',
    },
];

export const approvedStudent = [
    {
        title: 'STT',
        dataIndex: 'stt',
    },
    {
        title: 'Tài khoản',
        dataIndex: 'taiKhoan',
        align: 'center',
    },
    {
        title: 'Học viên',
        dataIndex: 'hoTen',
        align: 'center',
    },
    {
        title: 'Chờ xác nhận',
        dataIndex: 'action',
        align: 'center',
    },
];
