import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import loadingSlice from './react-toolkit/loadingSlice';
import successNotifySlice from './react-toolkit/successNotifySlice';
import warningNotifySlice from './react-toolkit/warningNotifySlice';
import UserWarningNotifySlice from './react-toolkit/UserWarningNotifySlice ';
import userLoginSlice from './react-toolkit/userLoginSlice';
import LoginWarningNotifySlice from './react-toolkit/LoginWarningNotifySlice';

import adminUserSlice from './react-toolkit/adminUserSlice';

const store = configureStore({
    reducer: {
        loadingSlice,
        successNotifySlice,
        warningNotifySlice,
        UserWarningNotifySlice,
        userLoginSlice,
        LoginWarningNotifySlice,
        adminUserSlice,
    },
});

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <Provider store={store}>
        <App />
    </Provider>,
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
